/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package occ.ues.edu.sv.ingenieria.prn335.controller;

import java.time.LocalDate;
import java.util.ArrayList;
import occ.ues.edu.sv.ingenieria.prn335.entity.Pelicula;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author marlon
 */
public class CineTest {

    public CineTest() {
    }

    @org.junit.jupiter.api.BeforeAll
    public static void setUpClass() throws Exception {
    }

    @org.junit.jupiter.api.AfterAll
    public static void tearDownClass() throws Exception {
    }

    @org.junit.jupiter.api.BeforeEach
    public void setUp() throws Exception {
    }

    @org.junit.jupiter.api.AfterEach
    public void tearDown() throws Exception {
    }

    /**
     * Test of agregarPelicula method, of class Cine.
     */
    @org.junit.jupiter.api.Test
    public void testAgregarPelicula() {
        System.out.println("agregarPelicula");
        //prueba cuando ingresa una clasificacion no permitida=E
        int idPelicula = 1;
        String titulo = "harry";
        String duracion = "110 minutos";
        String director = "james";
        String genero = "comedia";
        LocalDate fechaEstreno = LocalDate.of(2020, 12, 12);
        char clasificacion = 'E'; //probamos con una clasificacion mala que devuelva null
        String sinopsis = "loco";
        Cine instance = new Cine();
        Pelicula esperado = null ;//new Pelicula(idPelicula, titulo, duracion, director, genero, fechaEstreno, clasificacion, sinopsis);
        Pelicula result = instance.agregarPelicula(idPelicula, titulo, duracion, director, genero, fechaEstreno, clasificacion, sinopsis);
        assertEquals(esperado, result);
    
        //para probar cuando todos los valores son correctos, evaluaremos si el tamaño de la lista crecio, ya que originalmente solo hay 5 peliculas 
        clasificacion='A';
        int tamanio=6;
         esperado = new Pelicula(idPelicula, titulo, duracion, director, genero, fechaEstreno, clasificacion, sinopsis);
         result = instance.agregarPelicula(idPelicula, titulo, duracion, director, genero, fechaEstreno, clasificacion, sinopsis);
        assertEquals(tamanio, instance.getListaPeliculas().size());
    }
}

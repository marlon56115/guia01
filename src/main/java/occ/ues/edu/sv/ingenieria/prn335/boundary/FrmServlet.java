/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package occ.ues.edu.sv.ingenieria.prn335.boundary;

import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import occ.ues.edu.sv.ingenieria.prn335.controller.Cine;
import occ.ues.edu.sv.ingenieria.prn335.entity.Pelicula;

/**
 *
 * @author marlon
 */
@WebServlet(name = "FrmServlet", urlPatterns = {"/FrmServlet"})
public class FrmServlet extends HttpServlet {
  

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>probando el post</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet From request " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>probando el post</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet FrmServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

      //se declara el objeto afuera para que se guarden los objetos en el array
    Cine cine = new Cine();
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */

            //primero se obtiene si apreto el boton de agregar o de modificar
            String agregar = request.getParameter("agregar");
            String editar = request.getParameter("modificar");

            //se manda a llamar el arraylist con las peliculas
            ArrayList<Pelicula> pelis = new ArrayList<>();
            pelis = cine.getListaPeliculas();
            //se guardan todos los parametros que ingreso en el formulario
            int idPelicula = Integer.parseInt(request.getParameter("id_pelicula"));
            String titulo = request.getParameter("titulo");
            String duracion = request.getParameter("duracion");
            String director = request.getParameter("director");
            String genero = request.getParameter("genero");
            DateTimeFormatter formato = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            LocalDate fechaEstreno = LocalDate.parse(request.getParameter("estreno"), formato);
            String sinopsis = request.getParameter("sinopsis");
            char clasificacion = request.getParameter("clasificacion").charAt(0);
            //se comienza a imprimir la pagina web
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>mostrar peliculas</title>");
            out.println("</head>");
            out.println("<body>");
            //si apreto el boton "agregar" entra al if
            if (agregar != null) {
                try {
                    //agrega pelicula
                    cine.agregarPelicula(idPelicula, titulo, duracion, director, genero, fechaEstreno, clasificacion, sinopsis);
                } catch (Exception e) {
                    out.print("<h1>Error al agregar</h1>" + e.getMessage());
                }
                //informa de la insercion
                out.print("<a href='index.html'>volver</a><br/><br/>");
                out.print("<h2>se agrego la siguiente pelicula:</h2> </br>");
                out.print("id: " + idPelicula + "</br>");
                out.print("titulo: " + titulo + "</br>");
                out.print("duracion: " + duracion + "</br>");
                out.print("director: " + director + "</br>");
                out.print("genero: " + genero + "</br>");
                out.print("fecha de estreno: " + fechaEstreno + "</br>");
                out.print("clasificacion: " + clasificacion + "</br>");
                out.print("sinopsis: " + sinopsis + "</br></br></br>");
                //imprime la tabla con las peliculas del arraylist
                out.print("<table border='1' bgcolor='green' align='center'>");
                out.print("<tr bgcolor='white' align='center' > ");
                out.print("<td>");
                out.print("id");
                out.print("</td>");
                out.print("<td>");
                out.print("pelicula");
                out.print("</td>");
                out.print("<td>");
                out.print("duracion");
                out.print("</td>");
                out.print("<td>");
                out.print("director");
                out.print("</td>");
                out.print("<td>");
                out.print("genero");
                out.print("</td>");
                out.print("<td>");
                out.print("fecha");
                out.print("</td>");
                out.print("<td>");
                out.print("clasificacion");
                out.print("</td>");
                out.print("<td>");
                out.print("sinopsis");
                out.print("</td>");
                out.print("</tr>");
                for (int contador = 0; contador < pelis.size(); contador++) {
                    out.print("<tr>");
                    out.print("<td>");
                    out.print(pelis.get(contador).getIdPelicula());
                    out.print("</td>");
                    out.print("<td>");
                    out.print(pelis.get(contador).getTitulo());
                    out.print("</td>");
                    out.print("<td>");
                    out.print(pelis.get(contador).getDuracion());
                    out.print("</td>");
                    out.print("<td>");
                    out.print(pelis.get(contador).getDirector());
                    out.print("</td>");
                    out.print("<td>");
                    out.print(pelis.get(contador).getGenero());
                    out.print("</td>");
                    out.print("<td>");
                    out.print(pelis.get(contador).getFechaEstreno());
                    out.print("</td>");
                    out.print("<td>");
                    out.print(pelis.get(contador).getClasificacion());
                    out.print("</td>");
                    out.print("<td>");
                    out.print(pelis.get(contador).getSinopsis());
                    out.print("</td>");
                    out.print("</tr>");
                }
                out.print("</table>");
                //si presiono "Editar" estra en el if
            } else if (editar != null) {
                try {
                    //intenta modificar la pelicula
                    cine.modificarPelicula(idPelicula, titulo, duracion, director, genero, fechaEstreno, clasificacion, sinopsis);
                } catch (Exception e) {
                    out.print("<h1>Error al modificar</h1>" + e.getMessage() + "<br/>");
                }
                //avisa de la insercion
                out.print("<a href='index.html'>volver</a><br/><br/>");
                out.print("<h2>se modifico la pelicula de id = " + idPelicula + "</h2> </br>");
                out.print("titulo: " + titulo + "</br>");
                out.print("duracion: " + duracion + "</br>");
                out.print("director: " + director + "</br>");
                out.print("genero: " + genero + "</br>");
                out.print("fecha de estreno: " + fechaEstreno + "</br>");
                out.print("clasificacion: " + clasificacion + "</br>");
                out.print("sinopsis: " + sinopsis + "</br>");
                //dibuja la tabla con las peliculas del arraylist
                out.print("<table border='1' bgcolor='green' align='center'>");
                out.print("<tr bgcolor='white' align='center'>");
                out.print("<td>");
                out.print("id");
                out.print("</td>");
                out.print("<td>");
                out.print("pelicula");
                out.print("</td>");
                out.print("<td>");
                out.print("duracion");
                out.print("</td>");
                out.print("<td>");
                out.print("director");
                out.print("</td>");
                out.print("<td>");
                out.print("genero");
                out.print("</td>");
                out.print("<td>");
                out.print("fecha");
                out.print("</td>");
                out.print("<td>");
                out.print("clasificacion");
                out.print("</td>");
                out.print("<td>");
                out.print("sinopsis");
                out.print("</td>");
                out.print("</tr>");
                for (int contador = 0; contador < pelis.size(); contador++) {
                    out.print("<tr>");
                    out.print("<td>");
                    out.print(pelis.get(contador).getIdPelicula());
                    out.print("</td>");
                    out.print("<td>");
                    out.print(pelis.get(contador).getTitulo());
                    out.print("</td>");
                    out.print("<td>");
                    out.print(pelis.get(contador).getDuracion());
                    out.print("</td>");
                    out.print("<td>");
                    out.print(pelis.get(contador).getDirector());
                    out.print("</td>");
                    out.print("<td>");
                    out.print(pelis.get(contador).getGenero());
                    out.print("</td>");
                    out.print("<td>");
                    out.print(pelis.get(contador).getFechaEstreno());
                    out.print("</td>");
                    out.print("<td>");
                    out.print(pelis.get(contador).getClasificacion());
                    out.print("</td>");
                    out.print("<td>");
                    out.print(pelis.get(contador).getSinopsis());
                    out.print("</td>");
                    out.print("</tr>");
                }

                out.print("</table>");
            }
            out.println("</body>");
            out.println("</html>");
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
